<?php


namespace App\UI\Subscription\Http\Controllers\Mappers;

use App\Gifts\Gift;
use App\Organisations\Organisation;
use App\Subscriptions\Models\Unsubscription;
use App\Utils\Collection\Arrayable;

trait SubscriptionMapper
{
    /**
     * @param Gift $gift
     * @param bool $isFeedbackSent
     * @return Arrayable
     */
    public function unsubscribeGiftResponseMapper(Gift $gift, bool $isFeedbackSent): Arrayable
    {
        return new class($gift, $isFeedbackSent) implements Arrayable
        {
            /**
             * @var Gift $gift
             */
            private $gift;

            /**
             * @var bool $isFeedbackSent
             */
            private $isFeedbackSent;

            public function __construct(Gift $gift, bool $isFeedbackSent)
            {
                $this->gift           = $gift;
                $this->isFeedbackSent = $isFeedbackSent;
            }

            public function toArray(): array
            {
                $gift = $this->gift;

                $unsubscription = Unsubscription::query()->byEmail($gift->getGiftee()->getEmail())->first();

                return [
                    'sender'                => [
                        'full_name' => $gift->getFromUserObject()->full_name,
                    ],
                    'recipient'             => [
                        'id'        => $gift->getGiftee()->id,
                        'email'     => $gift->getGiftProcess()->email,
                        'full_name' => $gift->getGiftProcess()->getFullName(),
                    ],
                    'can_be_declined'       => $gift->canBeDeclinedDuringUnsubscribe(),
                    'is_unsubscribed'       => (bool)$unsubscription,
                    'company'               => $this->getGiftOrganisation($gift) ? $this->getGiftOrganisation($gift)->getName() : null,
                    'outer_unsubscribe_url' => $this->getGiftOrganisation($gift) ? $gift->getTeam()->getSetting('outer_unsubscribe_url') : null,
                    'is_feedback_sent'      => (bool)$unsubscription ?: $this->isFeedbackSent,
                ];

            }

            private function getGiftOrganisation(Gift $gift): ?Organisation
            {

                if (!$gift->hasTeam()) {
                    return null;
                }

                if (!$gift->getTeam()->hasOrganisation()) {
                    return null;
                }

                return $gift->getTeam()->getOrganisation();
            }
        };
    }


}
