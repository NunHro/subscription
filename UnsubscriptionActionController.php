<?php

namespace App\UI\Subscription\Http\Controllers;

use App\EventsLogs\EventsCollector;
use App\Gifts\Repositories\GiftRepository;
use App\Subscriptions\SubscriptionManager;
use App\Subscriptions\Validators\GiftDeclineFeedbackValidator;
use App\Subscriptions\Validators\UnsubscribeValidator;
use App\Events\Subscription\Segment\RecipientResubscribedSegmentEvent;
use App\Events\Subscription\Segment\RecipientUnsubscribedSegmentEvent;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;
use Log;
use DB;

class UnsubscriptionActionController extends UnsubscriptionControllerAbstract
{

    /**
     * @var SubscriptionManager
     */
    private $subscriptionManager;

    /**
     * @var EventsCollector
     */
    private $eventsCollector;

    public function __construct(
        Request $request,
        GiftRepository $giftRepository,
        SubscriptionManager $subscriptionManager,
        EventsCollector $eventsCollector
    )
    {
        parent::__construct($request, $giftRepository);
        $this->subscriptionManager = $subscriptionManager;
        $this->eventsCollector     = $eventsCollector;
    }

    public function postDecline(Request $request, GiftDeclineFeedbackValidator $reasonValidator): JsonResponse
    {
        $this->initGift();

        $feedback = $request->get('feedback');
        $reason = 'unsubscribe';
        $reasonValidator->validate(['feedback' => $feedback, 'reason' => $reason]);

        try {
            $this->subscriptionManager->declineSingleGift($this->gift, $reason, $feedback);
        } catch (Throwable $exception) {
            Log::error('Gift decline failed', [
                'module'    => 'subscription',
                'exception' => $exception,
                'error'     => $exception->getMessage(),
            ]);

            return $this->responseJsonFail([
                'global' => 'Can`t decline gift',
            ]);
        }

        $this->eventsCollector->fireEvents();

        return $this->responseJsonSuccess([
            'gift' => $this->unsubscribeGiftResponseMapper($this->gift, true)->toArray(),
        ])->withCookie($this->prepareFeedbackCookie(true));
    }

    public function postUnsubscribe(Request $request, UnsubscribeValidator $unsubscribeValidator): JsonResponse
    {
        $this->initGift();

        $feedback = $request->get('feedback');
        $unsubscribeValidator->validate(['feedback' => $feedback]);

        $contact = $this->gift->getGiftee();


        try {
            DB::beginTransaction();
            $unsubscription = $this->subscriptionManager->doUnsubscribe($contact, 'unsubscribe', $feedback);
            $this->eventsCollector->delayQueue(new RecipientUnsubscribedSegmentEvent($this->gift, $unsubscription));
            DB::commit();
        } catch (Throwable $exception) {

            Log::error('Recipient unsubscribe process failed', [
                'module'    => 'subscription',
                'exception' => $exception,
                'error'     => $exception->getMessage(),
            ]);
            DB::rollBack();

            return $this->responseJsonFail([
                'global' => 'Can`t unsubscribe.',
            ]);
        }

        $this->eventsCollector->fireEvents();

        return $this->responseJsonSuccess([
            'gift' => $this->unsubscribeGiftResponseMapper($this->gift, true)->toArray(),
        ])->withCookie($this->prepareFeedbackCookie(true));
    }

    public function postResubscribe(): JsonResponse
    {
        $this->initGift();

        $contact = $this->gift->getGiftee();

        try {
            $this->subscriptionManager->doResubscribe($contact);
            $this->eventsCollector->delayQueue(new RecipientResubscribedSegmentEvent($this->gift));
        } catch (Throwable $exception) {
            Log::error('Recipient resubscribe process failed', [
                'module'    => 'subscription',
                'exception' => $exception,
                'error'     => $exception->getMessage(),
            ]);

            return $this->responseJsonFail([
                'global' => 'Can`t resubscribe.',
            ]);
        }

        $this->eventsCollector->fireEvents();

        return $this->responseJsonSuccess([
            'gift' => $this->unsubscribeGiftResponseMapper($this->gift, false)->toArray(),
        ])->withCookie($this->prepareFeedbackCookie(false));
    }

}
