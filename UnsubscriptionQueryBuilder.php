<?php


namespace App\Subscriptions\Models;


use Illuminate\Database\Eloquent\Builder;


final class UnsubscriptionQueryBuilder extends Builder
{

    public function byEmail(string... $email)
    {
        return $this->whereIn('email', $email);
    }

}


