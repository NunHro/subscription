<?php

namespace App\UI\Subscription\Http\Controllers;


use App\Gifts\Gift;
use App\Gifts\Repositories\GiftRepository;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Log;
use Symfony\Component\HttpFoundation\Cookie;

class UnsubscriptionController extends Controller
{

    /**
     * @var GiftRepository
     */
    private $giftRepository;

    /**
     * @var string
     */
    private $unsubscribeUrl;

    public function __construct(GiftRepository $giftRepository)
    {
        $this->giftRepository = $giftRepository;
        $this->unsubscribeUrl = config('app.unsubscribe_spa_url');
    }

    public function processUnsubscribeLink(Request $request): RedirectResponse
    {
        $giftHashid = $this->getRouteParam('gift_hashid');
        $gift       = $this->giftRepository->getByHashid($giftHashid);

        if (!$gift) {
            Log::warning('Tried to access not existing gift unsubscribe page', [
                'url'        => $request->url(),
                'user_agent' => $request->userAgent(),
            ]);

            return redirect(action('HomeController@home'));
        }

        $unsubscribePass = $this->getRouteParam('unsubscribe_pass');
        if ($unsubscribePass !== $gift->getGifterPass()) {
            Log::warning('Tried to access gift unsubscribe page with wrong pass', [
                'url'          => $request->url(),
                'user_agent'   => $request->userAgent(),
                'allowed_pass' => $gift->getGifterPass(),
                'input_pass'   => $unsubscribePass,
            ]);

            return redirect(action('HomeController@home'));
        }

        return redirect($this->unsubscribeUrl)
            ->withCookie($this->prepareGiftCookie($gift))
            ->withCookie($this->prepareFeedbackCookie());
    }


    public function prepareGiftCookie(Gift $gift): Cookie
    {
        return new Cookie(
            Gift::GIFT_HASHID_COOKIE_KEY,
            $gift->getHashid(),
            Carbon::now()->addHours(2)
        );

    }

    protected function prepareFeedbackCookie(): Cookie
    {
        return new Cookie(
            'is_feedback_sent',
            false,
            Carbon::now()->addHours(2)
        );
    }
}
