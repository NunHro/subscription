<?php

namespace App\UI\Subscription\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UnsubscriptionDataController extends UnsubscriptionControllerAbstract
{

    public function getData(Request $request): JsonResponse
    {
        $this->initGift();

        $isFeedbackSent = (bool)$request->cookie('is_feedback_sent', false);

        return $this->responseJsonSuccess([
            'gift' => $this->unsubscribeGiftResponseMapper($this->gift, $isFeedbackSent)->toArray(),
        ]);
    }

}
