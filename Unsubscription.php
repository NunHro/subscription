<?php

namespace App\Subscriptions\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


/**
 * Class Unsubscription
 *
 * @package App\Subscriptions
 *
 * @property int    $id
 * @property string $email
 * @property string $feedback
 */
class Unsubscription extends Model
{
    protected $table    = 'unsubscriptions';

    protected $fillable = [
        'email',
        'feedback',
    ];


    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @return UnsubscriptionQueryBuilder|Builder|Model
     */
    public function newEloquentBuilder($query)
    {
        return new UnsubscriptionQueryBuilder($query);
    }

    /**
     * @return UnsubscriptionQueryBuilder|Builder
     */
    public static function query(): UnsubscriptionQueryBuilder
    {
        return parent::query();
    }

    public function getFeedback(): string
    {
        return $this->feedback;
    }

    public function isFeedbackSent(): bool
    {
        return (bool)$this->getFeedback();
    }
}
